/*
 * =====================================================================================
 *
 *       Filename:  bubble_sort1.c
 *
 *    Description:  the first bubble sort example.
 *                  page 9
 *
 *        Version:  1.0
 *        Created:  07/17/2014 11:19:00
 *       Revision:  none
 *       Compiler:  gcc/clang
 *
 *         Author:  (Steven Sun),
 *   Organization:
 *
 * =====================================================================================
 */
#include <stdlib.h>
#include <stdio.h>

int main(){
  int a[100], i, j, t, n;
  scanf("%d", &n);   //denotes that we will enter n numbers.

  /*read into the array n numbers.*/
  for(i=0; i<n; i++){
    scanf("%d", &a[i]);
  }

  for(i=0; i<n-1; i++){    //for n numbers, we just need to do n times.
    for(j=0; j<n-i; j++){
      if (a[j] < a[j+1]) {   //swap two numbers.
        t = a[j];
        a[j] = a[j+1];
        a[j+1] = t;
      }
    }
  }

  for(i=0; i<n;i++){
    printf("%d ", a[i]);
  }

  getchar();getchar();
  printf("\n");
  return 0;
}

/*the initial value of i in a loop need to conform to the same rule*/

