/*
 * =====================================================================================
 *
 *       Filename:  bucketSort1.c
 *
 *    Description:  桶排序，有5个同学，5个考分， 满分是10分，请把分数从大到小排序，假设分数是
 *                  5, 3, 5, 2, 8.
 *                  其实是对0到10之间的5个数进行排序。
 *
 *        Version:  1.0
 *        Created:  07/17/2014 09:04:53
 *       Revision:  none
 *       Compiler:  gcc/clang
 *
 *         Author:  (Steven Sun),
 *   Organization:
 *
 * =====================================================================================
 */
#include <stdlib.h>
#include <stdio.h>

int main(){
  int mark[11], i, j, t;

  for (i = 0; i < 11; i++){
      mark[i] = 0;
  }

  for (i = 0; i < 5; i++){
    scanf("%d", &t);
    mark[t]++;
  }

  for (i = 0; i < 11; i++){
     for (j = 0; j < mark[i]; j++){
       printf("%d ", i);
     }
  }

  printf("\n");
  return 0;
}

