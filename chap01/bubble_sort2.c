/*
 * =====================================================================================
 *
 *       Filename:  bubble_sort2.c
 *
 *    Description:  按照学生的分数来顺序打印出学生的名字。
 *                  page 10
 *
 *        Version:  1.0
 *        Created:  07/17/2014 11:43:45
 *       Revision:  none
 *       Compiler:  gcc/clang
 *
 *         Author:  (Steven Sun),
 *   Organization:
 *
 * =====================================================================================
 */
#include <stdlib.h>
#include <stdio.h>

struct student{
  char name[21];
  int score;
};

int main(){
  struct student a[1000], t;
  int i, j, n;
  scanf("%d", &n);

  for(i=1; i<=n; i++){
    scanf("%s %d", a[i].name, &a[i].score);
  }
  printf("And the sorted output is: \n");

    /*sort the numbers from large to small.*/
  for(i=1; i<=n-1; i++){
    for(j=1; j<=n-i; j++){
      if (a[j].score < a[j+1].score) {
        t = a[j];
        a[j] = a[j+1];
        a[j+1] = t;
      }
    }
  }

  for(i=1; i<=n; i++){
    printf("%s\n", a[i].name);
  }
  return 0;
}
