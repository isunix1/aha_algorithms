/*
 * =====================================================================================
 *
 *       Filename:  section4-ans1.c
 *
 *    Description:  小哼买书，page 22
 *
 *        Version:  1.0
 *        Created:  08/10/2014 14:11:15
 *       Revision:  none
 *       Compiler:  gcc/clang
 *
 *         Author:  (Steven Sun),
 *   Organization:
 *
 * =====================================================================================
 */
#include <stdlib.h>
#include <stdio.h>

int main(int argc, const char *argv[])
{
  int a[10001], n, i, t;
  for (i = 1; i <= 1000; i++) {
    a[i] = 0;
  }

  scanf("%d", &n);
  for (i = 1; i <= n; i++) {
    scanf("%d", &t);
    a[t] = 1;  //add flag to the one appeared.
  }

  for (i = 1; i <= 1000; i++) {
     if (a[i] == 1) {
       printf("%d ", i);
     }
  }

  return 0;
}
