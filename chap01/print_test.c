/*
 * =====================================================================================
 *
 *       Filename:  print_test.c
 *
 *    Description:  just a test for increment
 *
 *        Version:  1.0
 *        Created:  07/17/2014 09:08:03
 *       Revision:  none
 *       Compiler:  gcc/clang
 *
 *         Author:  (Steven Sun),
 *   Organization:
 *
 * =====================================================================================
 */
#include <stdlib.h>
#include <stdio.h>

int main(){
  int i, t;
  int a[6];
  for (i = 0; i < 5; i++) {
    scanf("%d ", &t);
    a[t]++;
  }

  printf("%d: ", t);
  printf("\n");
  printf("%d", a[t]);

}
