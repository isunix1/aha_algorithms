/*
 * =====================================================================================
 *
 *       Filename:  bucketSort2.c
 *
 *    Description:  输入n个0~1000之间的整数，将他们从大到小进行排序.
 *
 *        Version:  1.0
 *        Created:  07/17/2014 10:13:03
 *       Revision:  none
 *       Compiler:  gcc/clang
 *
 *         Author:  (Steven Sun),
 *   Organization:
 *
 * =====================================================================================
 */

#include <stdlib.h>
#include <stdio.h>

int main(){
  int mark[10001], i, j, t, n;

  for(i=0; i< 1001; i++){
  	mark[i] = 0;
  }

  scanf("%d\n", &n); 		/*use n to store the number of ints you want to sort*/

  for(j=0; j<n; j++){
  	scanf("%d", &t);
  	mark[t]++;
  }

  for(i=1001; i>0; i--){
  	for(j=0; j<mark[i]; j++){
  		printf("%d ", i);
  	}
  }

  printf("\n");
  return 0;
}


