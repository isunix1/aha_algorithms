/*
 * =====================================================================================
 *
 *       Filename:  aha_bucketSort1.c
 *
 *    Description:  just copy from the book
 *                  page 4
 *
 *        Version:  1.0
 *        Created:  07/17/2014 09:52:43
 *       Revision:  none
 *       Compiler:  gcc/clang
 *
 *         Author:  (Steven Sun),
 *   Organization:
 *
 * =====================================================================================
 */
#include <stdlib.h>
#include <stdio.h>

int main(){
  int a[11], i, j, t;
  for(i = 0; i <= 10; i++)
    a[i] = 0;

  for(i=1; i<=5; i++){
    scanf("%d", &t);
    a[t]++;
  }

  for(i=0; i<=10; i++)
    for(j=1; j<=a[i]; j++)
      printf("%d ", i);
  printf("\n");
  return 0;

}
