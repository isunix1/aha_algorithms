/*
 * =====================================================================================
 *
 *       Filename:  section4-ans2.c
 *
 *    Description:  page 23
 *
 *        Version:  1.0
 *        Created:  08/10/2014 14:17:25
 *       Revision:  none
 *       Compiler:  gcc/clang
 *
 *         Author:  (Steven Sun),
 *   Organization:
 *
 * =====================================================================================
 */
#include <stdlib.h>
#include <stdio.h>
int main(int argc, const char *argv[])
{
  int a[1001], n, i, j, t;
  scanf("%d", &n);
  for (i = 1; i <= n; i++) {
    scanf("%d", &a[i]);
  }

  for (i = 1; i <= n-1; i++) {
     for (j = 1; j <= n-i; j++) {
       if (a[j] > a[j+1]) {
         t = a[j];
         a[j] = a[j+1];
         a[j+1] = t;
       }
     }
  }

  printf("%d  ", a[1]);
  for(i = 2; i <= n; i++){
    if (a[i] != a[i-1]) {
      printf("%d  ", a[i])  ;
    }
  }

  return 0;
}
