/*
 * =====================================================================================
 *
 *       Filename:  sec3.c
 *
 *    Description:  page68, 火柴棍等式.
 *
 *        Version:  1.0
 *        Created:  08/10/2014 20:43:28
 *       Revision:  none
 *       Compiler:  gcc/clang
 *
 *         Author:  (Steven Sun),
 *   Organization:
 *
 * =====================================================================================
 */
#include <stdlib.h>
#include <stdio.h>

int fun(int x){
  int num = 0;
  int f[10] = {6, 2, 5, 5, 4, 5, 6, 3, 7, 6};
  while(x/10 != 0){
    num += f[x%10];
    x = x/10;
  }

  num += f[x];
  return num;
}

int main(int argc, const char *argv[])
{
  int a, b, c, m, i, sum=0;
  scanf("%d", &m);
  for (a = 0; i <= 1111; a++) {
     for (b = 0; b <= 1111; b++) {
       c = a + b;
     if (fun(a) + fun(b) + fun(c) == m-4) {
       printf("%d+%d=%d\n", a, b, c);
       sum++;
     }
    }
  }

  printf("we can get %d diffenent identities.", sum);
  return 0;
}
