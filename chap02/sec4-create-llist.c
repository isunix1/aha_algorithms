/*
 * =====================================================================================
 *
 *       Filename:  sec4-create-llist.c
 *
 *    Description:  page50
 *                  to create a linked list;
 *
 *        Version:  1.0
 *        Created:  08/10/2014 16:00:49
 *       Revision:  none
 *       Compiler:  gcc/clang
 *
 *         Author:  (Steven Sun),
 *   Organization:
 *
 * =====================================================================================
 */
#include <stdlib.h>
#include <stdio.h>

struct node {
  int data;
  struct node * next;
} /* optional variable list */;

int main(int argc, const char *argv[])
{
  struct node *head, *p, *q, *t;
  int i, n, a;
  scanf("%d", &n);

  head = NULL;
  for (i = 1; i <= n; i++) {
    scanf("%d", &a);
    p = (struct node *)malloc(sizeof(struct node));
    p->data = a;
    p->next = NULL;
    if (head == NULL) {
      head = p;
    } else {
      q->next = p;
    }

    q = p;
  }

  //print out the llist;
  t = head;
  while(t != NULL){
    printf("%d ", t->data);
    t = t->next;
  }

  return 0;
}
