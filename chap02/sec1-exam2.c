/*
 * =====================================================================================
 *
 *       Filename:  sec1-exam2.c
 *
 *    Description:  page30, using struct to realize a queue to solve the question:
 *                  if you enter the numbers "6 3 1 7 5 8 9 2 4", you will get
 *                  "6 1 5 9 4 7 2 8 3";
 *
 *        Version:  1.0
 *        Created:  08/10/2014 14:42:15
 *       Revision:  none
 *       Compiler:  gcc/clang
 *
 *         Author:  (Steven Sun),
 *   Organization:
 *
 * =====================================================================================
 */
#include <stdlib.h>
#include <stdio.h>

struct queue {
  int data[100];
  int head;
  int tail;
} /* optional variable list */;

int main(int argc, const char *argv[])
{
  struct queue q;
  int i;
  q.head = 1;
  q.tail = 1;

  for(i=1; i<=9; i++){
    scanf("%d", &q.data[q.tail]);
    q.tail++;
  }

  while(q.head < q.tail){
    printf("%d ", q.data[q.head]);
    q.head++;

    q.data[q.tail] = q.data[q.head];
    q.tail++;
    q.head++;
  }
  return 0;
}

