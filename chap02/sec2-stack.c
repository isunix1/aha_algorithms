/*
 * =====================================================================================
 *
 *       Filename:  sec2-stack.c
 *
 *    Description:  page 34, anagram.
 *
 *        Version:  1.0
 *        Created:  08/10/2014 14:54:28
 *       Revision:  none
 *       Compiler:  gcc/clang
 *
 *         Author:  (Steven Sun),
 *   Organization:
 *
 * =====================================================================================
 */

#include <stdio.h>
#include <string.h>

int main(int argc, const char *argv[])
{
  char a[101], s[101];
  int i, len, mid, next, top;

  gets(a);
  len = strlen(a);
  mid = len / 2 -1;

  top = 0;
  for (i = 0; i <= mid; i++) {
    s[++top] = a[i];
  }

  if (len % 2 == 0) {
    next = mid+1;
  } else {
    next = mid + 2;
  }

  //start matching;
  for (i = next; i <= len-1; i++) {
    if (a[i] != s[top]) {
      break;
    }
    top--;
  }

  //if the value of top is 0, it means all chars are matched.
  if(top == 0){
    printf("YES");
  } else {
    printf("NO");
  }
  return 0;
}
