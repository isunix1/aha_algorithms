/*
 * =====================================================================================
 *
 *       Filename:  sec3.c
 *
 *    Description:  page40, 小猫钓鱼或者火车接龙游戏。
 *
 *        Version:  1.0
 *        Created:  08/10/2014 15:24:22
 *       Revision:  none
 *       Compiler:  gcc/clang
 *
 *         Author:  (Steven Sun),
 *   Organization:
 *
 * =====================================================================================
 */
#include <stdlib.h>
#include <stdio.h>

struct queue
{
  int data[1000];
  int head;
  int tail;
};

struct stack {
  int data[10];
  int top;
} /* optional variable list */;

int main(int argc, const char *argv[])
{
  struct queue q1, q2;
  struct stack s;
  int flag[10];
  int i, t;

  q1.head = 1; q1.tail = 1;
  q2.head = 1; q2.tail = 1;

  //initialize the stack;
  s.top = 0;

  for (i = 1; i <= 9; i++) {
    flag[i] = 0;
  }

  for(i=1; i<=6; i++){
    scanf("%d", &q1.data[q1.tail]);
    q1.tail++;
  }


  for(i=1; i<=6; i++){
    scanf("%d", &q2.data[q2.tail]);
    q2.tail++;
  }

  while(q1.head < q1.tail && q2.head < q2.tail){
    t = q1.data[q1.head];
    if (flag[t] == 0) {
      q1.head++;
      s.top++;
      s.data[s.top] = t;
      flag[t] = 1;
    } else {
      q1.head++;
      q1.data[q1.tail] = t;
      q1.tail++;

      while(s.data[s.top] != t){
        flag[s.data[s.top]] = 0;  //cancle the flag;
        q1.data[q1.tail] = s.data[s.top];
        q1.tail++;
        s.top--;
      }
    }

    t = q2.data[q2.head];
    if (flag[t] == 0) {
      q2.head++;
      s.top++;
      s.data[s.top] = t;
      flag[t] = 1;
    } else {
      q2.head++;
      q2.data[q2.tail] = t;
      q2.tail++;
      while(s.data[s.top] != t){
        flag[s.data[s.top]] = 0;
        q2.data[q2.tail] = s.data[s.top];
        q2.tail++;
        s.top--;
      }
    }
  }

  if(q2.head == q2.tail){
    printf("jack wins\n");
    printf("what jack has now are: ");
    for (i = q1.head; i <= q1.tail - 1; i++) {
      printf(" %d", q1.data[i]);
    }

    if(s.top > 0){
      printf("\ncards ont the desktop are: ");
      for (i = 1; i <= s.top; i++) {
        printf(" %d", s.data[i]);
      }
    } else {
      printf("\nno cards on the desktop");
    }
  } else {
    printf("henry wins\n");
    printf("what henry has now are: ");
    for (i = q2.head; i <= q2.tail - 1; i++) {
      printf(" %d", q2.data[i]);
    }

    if(s.top > 0){
      printf("\ncards ont the desktop are: ");
      for (i = 1; i <= s.top; i++) {
        printf(" %d", s.data[i]);
      }
    } else {
      printf("\nno cards on the desktop");
    }

  }

  return 0;
}


