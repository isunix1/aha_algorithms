/*
 * =====================================================================================
 *
 *       Filename:  sec4-pointer.c
 *
 *    Description:  page 47
 *
 *        Version:  1.0
 *        Created:  08/10/2014 15:53:28
 *       Revision:  none
 *       Compiler:  gcc/clang
 *
 *         Author:  (Steven Sun),
 *   Organization:
 *
 * =====================================================================================
 */
#include <stdlib.h>
#include <stdio.h>

int main(int argc, const char *argv[])
{
  int *p;
  p = (int *)malloc(sizeof(int)) ;
  *p = 10;
  printf("%d\n", *p);
  return 0;
}
