/*
 * =====================================================================================
 *
 *       Filename:  sec1-exam1.c
 *
 *    Description:  page 29
 *
 *        Version:  1.0
 *        Created:  08/10/2014 14:31:50
 *       Revision:  none
 *       Compiler:  gcc/clang
 *
 *         Author:  (Steven Sun),
 *   Organization:
 *
 * =====================================================================================
 */
#include <stdio.h>
int main(int argc, const char *argv[])
{
  int q[102] = {0, 6, 3, 1, 7, 5, 8, 9, 2, 4}, head, tail;
  int i;
  head = 1;
  tail = 10;

  while(head < tail){
    //print out the head and move it out of the queue
    printf("%d  ", q[head]);
    head++;

    //add the new head to the tail of the queue
    q[tail] = q[head];
    tail++;
    head++;
  }

  return 0;
}
